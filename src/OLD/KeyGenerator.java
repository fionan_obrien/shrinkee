/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OLD;

/**
 *
 * @author Fionán
 */
public class KeyGenerator {

    static int wordCount = 0;
    static final char MIN_CHAR = '!'; //words with crazy chars too
    static final char MAX_CHAR = '}';
    static final char NULL_CHAR = '\u0000';
    static int depth = 1;
    static char[] levels = {MIN_CHAR, NULL_CHAR, NULL_CHAR, NULL_CHAR};
    static int level = 1;

    protected static String getNextKey() {
        //dpoes not account for overflow
        String key = "";

        //if this is in the first rung
        switch (level) {
            case 1:

                key += levels[0];
                //increasing only as odd 
                levels[0] += 2;
                if (levels[0] + 2 > MAX_CHAR) {
                    //even char only
                    levels[0] = MIN_CHAR + 1;
                    //set at odd position
                    levels[1] = MIN_CHAR;

                    level = 2;
                }

                break;
            case 2:

                key += levels[0];
                key += levels[1];
                //increasing only as odd
                levels[1] += 2;

                if (levels[1] + 2 > MAX_CHAR) {
                    levels[0] += 2;
                    levels[1] = MIN_CHAR;

                    if (levels[0] + 2 > MAX_CHAR) {
                        levels[0] = MIN_CHAR + 1;
                        levels[1] = MIN_CHAR + 1;
                        levels[2] = MIN_CHAR;

                        level = 3;
                    }
                }

                break;
            case 3:

                key += levels[0];
                key += levels[1];
                key += levels[2];

                //third level increases odd or even
                levels[2]++;

                if (levels[2] > MAX_CHAR) {

                    levels[1] += 2;
                    levels[2] = MIN_CHAR;

                    if (levels[1] + 2 > MAX_CHAR) {

                        levels[0] += 2;
                        levels[1] = MIN_CHAR + 1;
                        levels[2] = MIN_CHAR;

                    }

                }
            default:
                break;

        }

        wordCount++;
        return key;

    }
}
