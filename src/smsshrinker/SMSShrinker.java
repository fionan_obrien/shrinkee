/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsshrinker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fionán
 */
public class SMSShrinker {

    static LinkedHashMap<String, String> words = new LinkedHashMap();

    static LinkedHashMap<String, String> codes = new LinkedHashMap();

    public static ArrayList<String> message = new ArrayList();

    static boolean DEBUG = false;

    public static void main(String[] args) throws FileNotFoundException {

//        String test1 = "this, is a test.";
//        Database data = new Database();
//        Message.setDatasource(data);
//
//        Message mess = new EncodedMessage(test1);
//
//        System.out.println(mess);
//
//        mess = new DecodedMessage(mess.toString());
//
//        System.out.println(mess);
//        Database.init();
//
//        mapWords();
//
//        Database.insertWordQuick(words);
//        Key key = new Key("}}}");
////        KeyReader key2 = new KeyReader("\")");
////        KeyReader key3 = new KeyReader("\"\u0060A");
//        System.out.println(key.toString() + " = " + key.toIndex());
//        
//        
        
//        System.out.println(key2.s + " = " + key2);
//        System.out.println(key3.s + " = " + key3);
//
//        KeyGenerator2 num1 = new KeyGenerator2(5);
//        KeyGenerator2 num2 = new KeyGenerator2(50);
//        KeyGenerator2 num3 = new KeyGenerator2(5000);
//        
//           System.out.println(num1.i + " = " + num1);
//        System.out.println(num2.i + " = " + num2);
//        System.out.println(num3.i + " = " + num3);
        
        
        
        
        
        
    }

    private static void mapWords() throws FileNotFoundException {

        File file = new File("wiki-100k.txt");

        Scanner sc = new Scanner(file);

        int count = 0;

        String wordScan;
        String charset = "UTF-8"; // or what corresponds
        BufferedReader in;
        try {
            in = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), charset));
            while ((wordScan = in.readLine()) != null) {

                //skip comments
                if (wordScan.charAt(0) == '#' || wordScan.charAt(0) == ' ') {
                    continue;
                }
                try {
                    //gonna skip this if no good
                    byte[] array = wordScan.getBytes("UTF-8");

                    wordScan = new String(array, Charset.forName("UTF-8"));
                } catch (UnsupportedEncodingException ex) {
                    continue;
                }
                int wordLength = wordScan.length();

              //  String key = KeyGenerator.getNextKey();
                
                Key key = new Key(count);
             
                words.put(key.toString(),wordScan);

               // codes.put(wordScan, key.toString());

                count++;

            }

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SMSShrinker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SMSShrinker.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static String encode2(String orginal) {
        String[] splited = orginal.split("\\s+");
        String encoded = "";

        for (String s : splited) {
            String meep = Database.getKeyByValueStatic(s);
            encoded += meep;
            //   System.out.println(s + "   :  " + meep);
        }

        return encoded;
    }

    private static String encode(String orginal) {

        String[] splited = orginal.split("\\s+");

        String encoded = "";

        for (String s : splited) {
            String encode = codes.get(s);
            System.out.println("Orginal word " + s + " =  " + encode);

            encoded += encode;

        }
        System.out.println("");
        return encoded;
    }

    private static void buildDatabase() {

        Database.init();

    }

    private static void findWords(String s) {

        boolean wordFlag = false;

        String word;

        for (int i = 0; i < s.length(); i++) {
            //get the char
            char c = s.charAt(i);

            //is c even or odd
            if (c % 2 == 0) {

                //even
                if (wordFlag) {

                    word = s.substring(i - 1, i + 2);
                    message.add(word);
                    i++;
                    wordFlag = false;

                } else {

                    wordFlag = true;
                    continue;

                }

            } else {
                //odd

                if (wordFlag) {
                    //if the wordFlag has been triggered
                    word = s.substring(i - 1, i + 1);

                    message.add(word);
                    //reset
                    wordFlag = false;

                } else {
                    word = "" + c;
                    message.add(word);
                    //redundant reset
                    wordFlag = false;

                }

            }

        }

    }

    private static String decode2(String encoded) {
        findWords(encoded);
        String result = "";
        for (String s : message) {
            if (s.equalsIgnoreCase("NOT_FOUND")) {
                result += "NOT_FOUND";
            }
            result += Database.getWordByKey(s) + " ";

        }

        return result;
    }

    private static String decode(String encoded) {
        findWords(encoded);

        String result = "";
        for (String s : message) {

            result += words.get(s) + " ";

        }
        return result;

    }

    private static void printToFile() {

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("coded.txt", "UTF-8");

            for (Map.Entry<String, String> entry : words.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                writer.println(key + " " + value);

                // now work with key and value...
            }

            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SMSShrinker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SMSShrinker.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }

    }

}
