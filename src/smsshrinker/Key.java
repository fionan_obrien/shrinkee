/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsshrinker;

/**
 *
 * @author Fionán
 *
 * The new improved Key Object is a unique sequence of characters paired with a
 * String It now creates a code mathematically and therefore more accurately.
 * This allows for direct select queries on a ROW_ID on a Database
 *
 * VALID KEYS are: 1-3 chars long if 1 char only an ODD character allowed if 2
 * chars long only EVEN then ODD allowed IF 3 chars long EVEN EVEN and ODD/EVEN
 * allowed.
 *
 * The allowable character range is from '!' - '}' or from 33-125 (in INT)92 The
 * possible combinations are:
 *
 * 46 1 char keycodes 46*46 2 char keycodes 46*46*92 char keycodes 196_788
 * possible combinations
 *
 */
public final class Key {

    //******CONSTANTS*******
    static final char BASE_CHAR_ODD = '!';
    static final char BASE_CHAR_EVEN = '\"';
    // possible combinations 
    static final int ODD_OR_EVEN = '}' - '!';
    static final int ODD = ODD_OR_EVEN / 2;
    static final int EVEN = ODD;
    static final int ODD_OR_EVEN_BY_EVEN = ODD_OR_EVEN*EVEN;
    
    static final int BASE_LEVEL2 = ODD;
    static final int BASE_LEVEL3 = ODD *EVEN;
    //******END OF CONSTANTS****
    
    //Local variable
    private String keycode;
    private int index;
    //***END of Local variable

    public Key(String keycode) {
        this.keycode = keycode;
        switch (keycode.length()) {
            case 1:
                this.index = numLevel1();
                break;
            case 2:
                this.index = numLevel2();
                break;
            case 3:
                this.index = numLevel3();
                break;

        }
    }

    //Start convert String to number
    final int numLevel1() {

        char c = keycode.charAt(0);

        int i1 = (c - BASE_CHAR_ODD) / 2;

        return i1;
    }

    final int numLevel2() {
        int baseIndex = BASE_LEVEL2;
        char[] chars = keycode.toCharArray();
        int i1, i2;

        i1 = (chars[0] - (BASE_CHAR_EVEN)) / 2;
        i2 = (chars[1] - (BASE_CHAR_ODD)) / 2;

        i1 *= BASE_LEVEL2;
        //i2 = i2;

        return baseIndex + i1 + i2;

    }

    final int numLevel3() {
        int baseIndex = BASE_LEVEL3;

        int i1, i2, i3;
        //{c1,c2,c3}
        char[] chars = keycode.toCharArray();

        i1 = (chars[0] - BASE_CHAR_EVEN) / 2;
        i2 = (chars[1] - BASE_CHAR_EVEN) / 2;
        i3 = chars[2] - BASE_CHAR_ODD;

        i1 *= (ODD_OR_EVEN * ODD);
        i2 *= ODD_OR_EVEN;
        // i3 = i3;

        return baseIndex + i1 + i2 + i3;
    }

    //end of convert String to number
    public Key(int index) {

        this.index = index;

        if (index < BASE_LEVEL2) {

            this.keycode = codeLevel1();

        } else if (index < BASE_LEVEL3) {
            this.keycode = codeLevel2();
        } else {
            this.keycode = codeLevel3();
        }

    }

    //for converting from number to String
    final String codeLevel1() {
        char[] chars = {(char) (BASE_CHAR_ODD + (this.index * 2))};

        return new String(chars);

    }

    final String codeLevel2() {
        int base = BASE_LEVEL2;
        char[] chars = {BASE_CHAR_EVEN, BASE_CHAR_ODD};

        int adjust = this.index - base;

        int tens = adjust / BASE_LEVEL2;
        int ones = adjust % BASE_LEVEL2;

        chars[0] = (char) (chars[0] + (tens * 2));
        chars[1] = (char) (chars[1] + (ones * 2));

        return new String(chars);
    }

    final String codeLevel3() {
        int base = BASE_LEVEL3;

        char[] chars = {BASE_CHAR_EVEN, BASE_CHAR_EVEN, BASE_CHAR_ODD};

        int adjust = this.index - base;

        int huns = adjust / ODD_OR_EVEN_BY_EVEN;
        int hunsMod = adjust % ODD_OR_EVEN_BY_EVEN;

        int tens = hunsMod / ODD_OR_EVEN;
        int ones = hunsMod % ODD_OR_EVEN;

        chars[0] = (char) (chars[0] + (huns * 2));
        chars[1] = (char) (chars[1] + (tens * 2));
        chars[2] = (char) (chars[2] + ones);

        return new String(chars);
    }

    //end of convert from number to string
    @Override
    public String toString() {
        return keycode;
    }

    public String toIndex() {
        return "" + index;
    }

}
