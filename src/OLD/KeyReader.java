/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OLD;

/**
 *
 * @author Fionán
 */
public class KeyReader {

    final char BASE_CHAR_ODD = '!';
    final char BASE_CHAR_EVEN = '\"';
    final String s;
    int i;

    public KeyReader(String s) {
        this.s = s;
        switch (s.length()) {
            case 1:
                this.i = numLevel1();
                break;
            case 2:
                this.i = numLevel2();
                break;
            case 3:
                this.i = numLevel3();
                break;

        }
    }

    final int numLevel3() {
        int baseIndex = 2116;

        int i1, i2, i3;
        //{c1,c2,c3}
        char[] chars = s.toCharArray();

        i1 = (chars[0] - BASE_CHAR_EVEN) / 2;
        i2 = (chars[1] - BASE_CHAR_EVEN) / 2;
        i3 = chars[2] - BASE_CHAR_ODD;

        i1 *= (92 * 46);
        i2 *= 92;
        i3 = i3;

        int index = baseIndex + i1 + i2 + i3;

        return index;
    }

    final int numLevel1() {

        char c = s.charAt(0);

        int i1 = (c - BASE_CHAR_ODD) / 2;

        return i1;
    }

    final int numLevel2() {
        int baseIndex = 46;
        char[] chars = s.toCharArray();
        int i1, i2;

        i1 = (chars[0] - (BASE_CHAR_EVEN)) / 2;
        i2 = (chars[1] - (BASE_CHAR_ODD)) / 2;

        i1 *= 46;
        i2 = i2;

        int index = baseIndex + i1 + i2;

        return index;

    }

    @Override
    public String toString() {
        return ""+i;
    }
    
    
}
