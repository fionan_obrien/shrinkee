/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsshrinker;

import com.fionan.dev.smsshrinker.message.Datasource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.SQLiteConfig;

/**
 *
 * @author Fionán
 */
public class Database implements Datasource {

    public static void init() {
        System.out.println("init Called");
        setUpTables();
        openConnection();
        setUpMetaData();
    }
    static Connection c = null;

    static {
        openConnection();
    }

    private static void setUpTables() {
        Statement stmt;
        try {
            if (c == null) {
                System.out.println("c = null");
                return;
            }

            stmt = c.createStatement();
            String sql = "CREATE TABLE if not exists words "
                    + "(_id INTEGER PRIMARY KEY   AUTOINCREMENT  ,"
                    + " KEY           TEXT    NOT NULL, "
                    + " VALUE          TEXT     NOT NULL) ";

            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Table created successfully");

    }

    private static void setUpMetaData() {

        Statement stmt;
        try {
            if (c == null) {
                System.out.println("c = null");
                return;
            }

            stmt = c.createStatement();
            String sql = "CREATE TABLE if not exists android_metadata "
                    + "(locale TEXT DEFAULT   en_US) ";

            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Table created successfully");

    }

    public static boolean insertWordQuick(LinkedHashMap<String, String> words) {
        //     PreparedStatement stmt = new PreparedStatement("INSERT INTO WORDS (key,value) values(?,?)");
        PreparedStatement insert = null;

        String insertString = "INSERT INTO words (key,value) values (?,?)";

        openConnection();
        try {
            if (c == null) {
                System.out.println("c = null");
                return false;
            }
            c.setAutoCommit(false);

            insert = c.prepareStatement(insertString);
            int i = 1;
            for (Map.Entry<String, String> e : words.entrySet()) {

                insert.setString(1, e.getKey());
                insert.setString(2, e.getValue());

                insert.addBatch();

                if (i % 100 == 0) {

                    insert.executeBatch();
                    c.commit();
                    insert.clearBatch();
                }

            }

            insert.executeBatch();
            c.commit();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
//        System.out.println("Records created successfully");
        return true;
    }

    protected static boolean openConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);

            c = DriverManager.getConnection("jdbc:sqlite:smsShrinker.db", config.toProperties());

            Statement s;
            s = c.createStatement();
            s.execute("PRAGMA foreign_keys=1;");

        } catch (ClassNotFoundException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            return false;
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            return false;
        }
        //       System.out.println("Opened connection successfully");
        return true;
    }

    protected static boolean closeConnection() {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return true;
    }

    //inefficent
    public static String getWordByKey(String key) {

        PreparedStatement select;

        String sql = "SELECT Value FROM words WHERE key =? limit 1";

        openConnection();

        try {
            select = c.prepareStatement(sql);
            select.setString(1, key);
            ResultSet rs = select.executeQuery();
            String value = rs.getString("value");
            c.close();
            return value;

        } catch (SQLException ex) {
            return "NOT_FOUND";
        }

    }

    //more efficent as this pulls only the row
    private static String getWordByKey(int key) {

        //        sql
        String sql = "SELECT Value FROM words WHERE ID = " + key + " COLLATE NOCASE";
        openConnection();
        Statement s;

        try {
            s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            String value = rs.getString("value");
            c.close();
            return value;

        } catch (SQLException e) {
            return "NOT_FOUND";
        }
    }

    

    public static String getKeyByValueStatic(String word) {

        PreparedStatement select;

        String sql = "SELECT key FROM words WHERE value =? collate nocase limit 1";

        openConnection();

        try {
            select = c.prepareStatement(sql);
            select.setString(1, word);
            ResultSet rs = select.executeQuery();
            String key = rs.getString("key");
            c.close();
            return key;

        } catch (SQLException ex) {
            return word;
        }
    }
@Override
    public String getKeyByValue(String word) {

        return getKeyByValueStatic(word);
    }
    @Override
    public String getValueByKey(String s) {
        return getWordByKey(s);

    }

    @Override
    public String getValueByKeyID(String s) {
        return getKeyByValue(s);
    }

}
