/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OLD;

/**
 *
 * @author Fionán
 */
public class KeyGenerator2 {

    final char BASE_CHAR_ODD = '!';
    final char BASE_CHAR_EVEN = '"';
    String s;
    int i;

    public KeyGenerator2(int index) {

        this.i = index;

        if (i < 46) {

            this.s = codeLevel1();

        } else if (i < 2116) {
            this.s = codeLevel2();
        } else {
            this.s = codeLevel3();
        }

    }

    private String codeLevel1() {
        char[] chars = {(char) (BASE_CHAR_ODD + (i * 2))};

        return new String(chars);

    }

    private String codeLevel2() {
        int base = 46;
        char[] chars = {BASE_CHAR_EVEN, BASE_CHAR_ODD};

        int adjust = i - 46;

        int tens = adjust / 46;
        int ones = adjust % 46;

        chars[0] = (char) (chars[0] + (tens * 2));
        chars[1] = (char) (chars[1] + (ones * 2));

        return new String(chars);
    }

    private  String codeLevel3() {
        int base = 2116;

        char[] chars = {BASE_CHAR_EVEN, BASE_CHAR_EVEN, BASE_CHAR_ODD};

        int adjust = i - 2116;

        int huns = adjust / 4232;
        int hunsMod = adjust % 4232;

        int tens = hunsMod / 92;
        int ones = hunsMod % 92;

        chars[0] = (char) (chars[0] + (huns * 2));
        chars[1] = (char) (chars[1] + (tens * 2));
        chars[2] = (char) (chars[2] + ones);

        return new String(chars);
    }

    @Override
    public String toString() {
        return s;
    }
    
    
    
}
